# NCPC 17 Solutions

This repo contains solutions to the 7 "easiest" problems in
[NCPC 2017](https://ncpc.idi.ntnu.no/ncpc2017/). My team (~10^42 % Mer
Kräm) solved 6 of them during the contest and I decided to release
them (a bit updated because writing with a time limit creates ugly
code &#127881;). I have also included solution description in every
problem except the easiest one (because it was a "do this" kind of
problem). Please do not submit these to Kattis, it will show up as a
duplicate and if I think that people straight up submit this I will
shut down the repo. It is however okay to find inspiration in this in
order to understand how to solve the problems.

Remember: The goal is to become a better programmer and problem
solver, use the problem solutions appropriately.



### A note on C++

For competitive programming uses there is are two things that might be
interesting, the <bits/stdc++.h> include will include EVERY possible
include from the standardlibrary (so you have, vectors,
priority_queue, cin/cout etc. just like that) and
`ios::sync_with_stdio(false)` and `cin.tie(nullptr)` will make input
output faster (but for large amounts of data less human friendly).
