/**
 * Problem description:
 *
 * Given a set of kayaks, a number of beginner, normal and experienced
 * kayakers, Find a way to put the kayakers into the kayaks so that
 * the slowest one is as fast as possible. The speed is determined by
 * the sum of the strengths multiplied by the speed of the kayak.
 *
 * Input sizes:
 *
 * B, N, E will sum up to a number between 2 and 100 000, there will
 * be half as many kayaks (two people in each). A kayaks speed is
 * between 1 and 100 000.
 *
 * Solution:
 *
 * The problem is solved by asking the question "Given a speed, v, is
 * it possible to determine if there is a placement such that the
 * slowest one is faster than v?". This is definitely possible by
 * greedily assigning those that are "just good enough" to the slowest
 * kayaks and working your way to the faster kayaks. This would be a
 * function that runs in O(C), the number of kayaks. We can also see
 * that this is a boolean function and thus we can use binary search
 * to query and find the highest speed such that we can find a
 * placement.
 *
 */
#include <bits/stdc++.h>
using namespace std;

using Int = long long;
using vi = vector<Int>;

/* Returns the speed which the slowest kayakers travel, or -1 if it is
   not possible to travel >= target */
Int works(const vi& Kayaks, vi people,
          const vi& Speeds, const Int Target) {

    struct Node {
        Int speed;
        int A, B;
        bool operator<(const Node& rhs) const { return speed < rhs.speed; }
    };

    Int slowest = 2000 * 100000;
    for (Int K : Kayaks) {
        vector< Node > choices;

        for (int A = 0; A < 3; ++A) { // A is the first person, B is the second
            for (int B = A; B < 3; ++B) {
                people[A]--; people[B]--;
                const Int Speed = K * (Speeds[A] + Speeds[B]);
                if (people[A] >= 0 and people[B] >= 0 and Speed >= Target) {
                    choices.push_back({Speed, A, B});
                }
                people[A]++; people[B]++;
            }
        }

        if (choices.size() == 0) {
            return -1;
        }
        auto res = min_element(choices.begin(), choices.end());
        slowest = min(slowest, res->speed);
        people[res->A]--; people[res->B]--;
    }
    return slowest;
}



int main() {
    cin.tie(nullptr);
    ios::sync_with_stdio(false);
    Int B, N, E;
    cin >> B >> N >> E;
    Int Bs, Ns, Es;
    cin >> Bs >> Ns >> Es;

    Int m = (B + N + E) / 2;
    vi kayaks(m);;
    for (auto& k : kayaks) cin >> k;

    vi people{B, N, E};
    vi speeds{Bs, Ns, Es};

    sort(kayaks.begin(), kayaks.end());

    Int low = 1;
    Int high = 2000 * 100000;
    // Binary search is hard... make the difference really small and then iterate through answers
    while (high - low > 1) {
        Int target = (low + high) / 2;
        bool ok = works(kayaks, people, speeds, target) == -1;
        (ok ? high : low) = target;
    }

    for (Int l = low; l <= high; ++l) {
        Int s = works(kayaks, people, speeds, l);
        if (s != -1) {
            cout << s << endl;
            break;
        }
    }
}
