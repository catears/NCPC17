# Problem Description:
#
# Given a number of teams and a timeline of how they solved GCPC
# (NCPC) problems, determine at each step what your favourite teams
# position is.
#
# Input Sizes:
#
# The number of teams are at max 10**5 and the number of events are at
# max 10**5 (where event means a problem solved by a team and a
# penalty time)
#
# Solution:
#
# This was ranked as the third easiest problem in the problem set but
# my team only solved it after the competition, unfortunately. The
# solution is rather straightforward, but we got caught up in
# analyzing worst case performance without taking into consideration
# the limits of the problem (an O(N**2) solution can work, but we did
# not figure that out).
#
# The key to solving the problem is keeping separate track of those
# that are above you in ranking and those that are below you in rank.
# This divides the events into 4 categories:
#
# a) You solve a problem
# b) Someone with less rank than you solve a problem and now have a better rank than you
# c) Someone with a higher rank than you solve a problem
# d) Someone with a lower rank than you solve a problem but it is not enough to surpass you
#
# We keep track of the ones that are above us so in a) we simply
# recompute those that have better rank. With b) we need to add them
# to the set of teams that are above us. For c) we need to update
# their entry in the set (remove + add new one). And for d) we simply
# "do nothing". In all cases we keep track (in an array) the total
# number of problems solved and the total penalty. The elements in the
# array represents the (Solutions, Penalty) tuple of each team and is
# used for comparison and updating purposes.
#
# What might surprise you is that this is "technically" an O(NM)
# algorithm because recalculating the number of teams above us means
# an O(N) operation in worst case and doing this M times would result
# in O(NM) complexity. However, the problem is constructed so that for
# N = M = 10**5 we can only do this once and not M times as such it
# becomes much similar to an O(N log N) in practice. This is the key
# that our team missed.
#
from sys import stdin

n, m = map(int, stdin.readline().split())

above = set()
teams = [(0, 0, idx) for idx in range(n)]

def add(t, p):
    return (teams[t][0] + 1, teams[t][1] + p, t)

def cmp(a, b):
    return (a[0], -a[1]) < (b[0], -b[1])

def eq(a, b):
    return not cmp(a, b) and not cmp(b, a)

for _ in range(m):
    t, p = map(int, stdin.readline().split())
    t -= 1
    # a) Team is us: Up us and remove anyone that is below us in the list
    # b) Team is above us: Remove old team values and add them again
    # c) Team is below us and are now above us: We lose a rank and they get added
    old = teams[t]
    new = add(t, p)
    if t == 0:
        above = set([k for k in above if cmp(new, k)])
    elif old in above:
        above.remove(old)
        above.add(new)

    elif (cmp(old, teams[0]) or eq(old, teams[0])) and cmp(teams[0], new):
        above.add(new)

    teams[t] = new

print(len(above) + 1)
