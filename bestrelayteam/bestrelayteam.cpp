/**
 * Problem Description:
 *
 * Given a number of runners and the time it takes for them to run a
 * lap with a flying start versus a standing start; Find the best
 * group of 4.
 *
 * Input Sizes:
 *
 * There will be up to 500 runners, a minimum of 4. Their speeds will be in the range of [8,20)
 *
 * Solution:
 *
 * The important thing to note is that the only runner that makes a
 * difference is the one that does not do a flying start, the first
 * runner. Apart from that runner you want to have the three fastest
 * (apart from the one you chose). That is; given the first runner we
 * choose the three runners that make the fastest non-first lap.
 *
 * The algorithm is rather simple and you keep a list of all runners,
 * sorted in order of flying start. Then you iterate through every
 * runner, pick them as runner of first lap and choose three from the
 * start of the sorted list.
 */
#include <bits/stdc++.h>
using namespace std;

struct Dude {
    int idx;
    string name;
    double a, b;
    bool operator<(const Dude& rhs) const { return b < rhs.b; }
};

int main() {
    int N; cin >> N;

    vector<Dude> dudes;
    for (int idx = 0; idx < N; ++idx) {
        string a;
        double b, c;
        cin >> a >> b >> c;
        dudes.push_back({idx, a, b, c});
    }
    vector<Dude> sorted(dudes);
    sort(sorted.begin(), sorted.end());

    double best_time = 1000000000.0;
    vector<string> best_names;

    for (Dude dude : dudes) {
        double time = dude.a;
        vector<string> names{dude.name};

        int count = 1;
        int search = 0;
        while (count < 4) {
            if (sorted[search].idx != dude.idx) {
                time += sorted[search].b;
                names.push_back(sorted[search].name);
                count++;
            }
            search++;
        }

        if (time < best_time) {
            best_time = time;
            best_names = names;
        }
    }

    cout << best_time << endl;
    for (auto s : best_names) {
        cout << s << endl;
    }
}
