# Problem Description:
#
# Given a files and their import structure, find the shortest cyclic
# dependency and print it (in any correct order).
#
# Input Sizes:
#
# Each program can be up to 8 lowercase letters (a-z). There will be
# at most 500 files.
#
# Solution:
#
# The files create a graph with one way edges. Fortunately N is very
# low (just 500). This means that we can easily do a bfs search for
# every node in the graph and find the shortest way back to
# itself. You also have to catch some special cases such as there not
# being a path. You also have to do some string handling. For these
# two reasons we implemented the solution in python, simply because
# the parse_import function becomes three lines long, while in C++ you
# would have to resort to almost black magic incantations
from sys import stdin

n = int(stdin.readline())
programs = [x for x in stdin.readline().split()]

imports = dict((x, []) for x in stdin.readline().split())

def parse_import(s):
    s = s[len("import"):]
    things = s.split(",")
    return [x.strip() for x in things]

for _ in range(n):
    name, lines = stdin.readline().split()
    lines = int(lines)
    for _ in range(lines):
        imports[name] += parse_import(stdin.readline().strip())

def bfs(start):
    visited = set([start])
    parent = {start: -1}
    Q = 0
    queue = [start]
    # Here we need to use a while loop because python does not have a
    # good way to traverse a list that dynamically changes....
    while Q < len(queue):
        now = queue[Q]
        Q += 1
        for friend in imports[now]:
            if friend not in visited:
                visited.add(friend)
                queue.append(friend)
                parent[friend] = now
            elif friend == start:
                path = [now]
                while path[-1] != -1:
                    path.append(parent[now])
                    now = parent[now]
                path.pop() # Remove -1 that exist after while loop
                return path[::-1] # The path will be reversed so reverse it
    return None

found = False
smallest = ['8'] * 1000
for key in imports:

    # For the simplest case (imports itself) just print it
    if key in imports[key]:
        print(key)
        exit()
    path = bfs(key)
    if path is None:
        continue
    found = True
    if len(path) < len(smallest):
        smallest = path

if not found:
    print("SHIP IT")
else:
    print(" ".join(smallest))
