/**
 * Problem Description:
 *
 * Given a height map (with negatives, where negative means water),
 * find the total amount of water that is drained if you place a
 * drainer at a specific point.
 *
 * Input sizes:
 *
 * The map is at most 500x500 in size. The height can be from -10**6
 * to 10**6.
 *
 * Solution:
 *
 * The key to solving this problem is handling underwater ridges
 * correctly. You always want to explore the "deepest" parts
 * first. This means that the problem has a natural conversion to
 * Dijkstras algorithm. Dijkstra normally operates in order of
 * increasing distance, but you can easily tweak it to operate in
 * order of deepest first. This makes the "smallest distance first"
 * search turn into a "depth first search". Efficiently adding and
 * popping the lowest tile is done by using a priority queue.
 */
#define let const auto
#include <bits/stdc++.h>
using namespace std;

using L = int64_t;
using vvl = vector<vector<L>>;
using Pair = pair<L, L>;

L& Row(Pair& p) { return p.first; }
L& Col(Pair& p) { return p.second; }
const L& Row(const Pair& p) { return p.first; }
const L& Col(const Pair& p) { return p.second; }

int main () {
    vvl baltic;
    int w, h;
    cin >> h >> w;
    let NumRow = h;
    let NumCol = w;
    baltic.assign(NumRow, vector<L>(NumCol, 0));
    vvl visited = baltic;


    for (auto& row : baltic) {
        for (auto& cell : row) cin >> cell;
    }

    auto cmp = [&](Pair lhs, Pair rhs) {
        return baltic[Row(lhs)][Col(lhs)] > baltic[Row(rhs)][Col(rhs)];
    };
    priority_queue<Pair, vector<Pair>, decltype(cmp)> pq(cmp);


    int start_row, start_col;
    cin >> start_row >> start_col;
    start_row--; // to 0-index
    start_col--;

    L current_sink = baltic[start_row][start_col];
    L total_displacement = 0;

    pq.push({start_row, start_col});

    // Depth first search
    while(!pq.empty()) {
        let top = pq.top(); pq.pop();
        if (visited[Row(top)][Col(top)]) {continue;}
        if (baltic[Row(top)][Col(top)] >= 0) {break;}

        visited[Row(top)][Col(top)] = 1;
        current_sink = max(current_sink, baltic[Row(top)][Col(top)]);
        total_displacement -= current_sink;

        // Used to walk to all 8 adjacent squares
        const vector<int> rd {-1, 0, 1,-1, 1,-1, 0, 1};
        const vector<int> cd {-1,-1,-1, 0, 0, 1, 1, 1};
        for (int i {}; i < 8; ++i) {
            let row = top.first + rd[i];
            let col = top.second + cd[i];
            if (col < NumCol && col >= 0 && row < NumRow && row >= 0) {
                pq.push({row,col});
            }
        }
    }
    cout << total_displacement << endl;
}
