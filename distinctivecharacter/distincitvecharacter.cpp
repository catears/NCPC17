/**
 * Problem Description:
 *
 * Given sets of attributes (on/off). Determine the set of attributes
 * to which the minimal distance is maximized.
 *
 * Input sizes:
 *
 * There will be at most 10**5 attribute combinations. There will be
 * at most 20 different attributes.
 *
 * Solution:
 *
 * Attributes are represented as binary strings, this gives a hint
 * that there are somehow 2**k states. If we think of the problem as a
 * graph problem we can easily find a description of the
 * solution. Imagine that there is a graph each labeled with a binary
 * string of length K. Since this is a graph we want to find the node
 * with the maximum distance to any other source (player) node. Given
 * a node, what are the adjacent nodes? Those which differ in only one
 * bit.
 *
 * With all of the above information we can construct a solution. Mark
 * each node that has a player in it. Continue with a leveled BFS from
 * all nodes where a player was placed. Run through all nodes and find
 * the one with the highest level, this will be furthest away from any
 * other player.
 */
#define let const auto
#include <bits/stdc++.h>
using namespace std;

struct Node {
    int idx{};
    int level{};
    bool operator<(const Node& rhs) { return level < rhs.level; }
    bool operator>(const Node& rhs) { return level > rhs.level; }
};

void print(int n_attr, int best_idx) {
    for (int i = n_attr - 1; i >= 0; --i) {
        let filtered = (1 << i) & best_idx;
        if (filtered > 0) {
            cout << '1';
        } else {
            cout << '0';
        }
    }
    cout << endl;
}

int main() {
    int players, attrs;
    cin >> players >> attrs;

    let K = attrs;
    let NumNodes = 1 << K; // 2 ** k

    vector<int> queue;
    vector<int> visited(NumNodes);
    vector<Node> nodes(NumNodes);
    for (int i = 0; i < NumNodes; ++i) nodes[i].idx = i;

    for (int i { 0 }; i < players; ++i) {
        string tmp; cin >> tmp;
        int cnt = 0;
        for (char c : tmp) {
            (c == '1') ? (cnt |= 1) : 0;
            cnt <<= 1;
        }
        cnt >>= 1;
        if (!visited[cnt]) {
            visited[cnt] = true;
            queue.push_back(cnt);
        }
    }

    int level = 0;
    while (not queue.empty()) {
        vector<int> next;
        for (int space : queue) {
            nodes[space].level = level;
            for (int i = 0; i < K; ++i) {
                let neigh = (1 << i) ^ space;
                if (!visited[neigh]) {
                    visited[neigh] = true;
                    next.push_back(neigh);
                }
            }
        }
        queue = next;
        level++;
    }


    let best_idx = max_element(begin(nodes), end(nodes))->idx;
    print(attrs, best_idx);
}
