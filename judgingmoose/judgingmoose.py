# The easiest problem in the problem set, solution is simple.
from sys import stdin

a, b = map(int, stdin.readline().split())

parity = "Odd" if a != b else "Even"
total = 2 * max(a, b)

if not total:
    print("Not a moose")
else:
    print(parity, total)




